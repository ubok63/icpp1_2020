#include "Datum.h"

Datum::Datum()
{
	this->den = 0;
	this->mesic = 0;
	this->rok = 0;
}

Datum::Datum(int den, int mesic, int rok)
{
	this->den = den;
	this->mesic = mesic;
	this->rok = rok;
}

Datum::~Datum()
{
}

void Datum::setDen(int den)
{
	this->den = den;
}

void Datum::setMesic(int mesic)
{
	this->mesic = mesic;
}

void Datum::setRok(int rok)
{
	this->rok = rok;
}

int Datum::getDen() const
{
	return this->den;
}

int Datum::getMesic() const
{
	return this->mesic;
}

int Datum::getRok() const
{
	return this->rok;
}



std::ostream & operator<<(std::ostream & output, const Datum & d)
{
	output << d.getDen << " " << d.getMesic << " " << d.getRok << std::endl;
	return output;
}

std::istream & operator>>(std::istream & input, Datum & d)
{
	input >> d.getDen;
	input >> d.getMesic;
	input >> d.getRok;
	return input;
}
