#include "Adresa.h"

Adresa::Adresa()
{
}

Adresa::Adresa(std::string ulice, std::string mesto, int psc)
{
	this->ulice = ulice;
	this->mesto = mesto;
	this->psc = psc;
}

Adresa::~Adresa()
{
}

void Adresa::setUlice(std::string ulice)
{
	this->ulice = ulice;
}

void Adresa::setMesto(std::string mesto)
{
	this->mesto = mesto;
}

void Adresa::setPsc(int psc)
{
	this->psc = psc;
}

std::string Adresa::getUlice() const
{
	return this->ulice;
}

std::string Adresa::getMesto() const
{
	return this->mesto;
}

int Adresa::getPsc() const
{
	return this->psc;
}



std::ostream & operator<<(std::ostream & output, const Adresa & a)
{
	output << a.getMesto << " " << a.getUlice << " " << a.getPsc << std::endl;
	return output;
}

std::istream & operator>>(std::istream & input, Adresa & a)
{
	input >> a.getMesto;
	input >> a.getUlice;
	input >> a.getPsc;
	return input;
}
