#ifndef ADRESA_H
#define ADRESA_H
#include <string>
#include <iostream>


struct Adresa
{
private:
	std::string ulice;
	std::string mesto;
	int psc;

public:
	Adresa();
	Adresa(std::string ulice, std::string mesto, int psc);
	~Adresa();
	void setUlice(std::string ulice);
	void setMesto(std::string mesto);
	void setPsc(int psc);
	std::string getUlice() const;
	std::string getMesto() const;
	int getPsc() const;

};

std::ostream& operator<<(std::ostream& output, const Adresa& a);
std::istream& operator>>(std::istream& input, Adresa& a);

#endif ADRESA_H