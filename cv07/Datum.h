#ifndef DATUM_H
#define DATUM_H

#include <iostream>
struct Datum
{
private:
	int den;
	int mesic;
	int rok;

public:
	Datum();
	Datum(int den, int mesic, int rok);
	~Datum();
	void setDen(int den);
	void setMesic(int mesic);
	void setRok(int rok);
	int getDen() const;
	int getMesic() const;
	int getRok() const;
};

std::ostream& operator<<(std::ostream& output, const Datum& d);
std::istream& operator>>(std::istream& input, Datum& d);

#endif DATUM_H