#include "Osoba.h"
#include <iostream>
#include <ostream>
#include <fstream>

Osoba::Osoba()
{
}

Osoba::Osoba(std::string jmeno, std::string prijmeni, Adresa trvaleBydliste, Datum datumNarozeni)
{
	this->jmeno = jmeno;
	this->prijmeni = prijmeni;
	this->trvaleBydliste = trvaleBydliste;
	this->datumNarozeni = datumNarozeni;
}

Osoba::~Osoba()
{
}

void Osoba::setJmeno(std::string jmeno)
{
	this->jmeno = jmeno;
}

void Osoba::setPrijmeni(std::string prijmeni)
{
	this->prijmeni = prijmeni;
}

void Osoba::setTrvaleBydliste(Adresa trvaleBydliste)
{
	this->trvaleBydliste = trvaleBydliste;
}

void Osoba::setDatumNarozeni(Datum datumNarozeni)
{
	this->datumNarozeni = datumNarozeni;
}

std::string Osoba::getJmeno() const
{
	return this->jmeno;
}

std::string Osoba::getPrijmeni() const
{
	return this->prijmeni;
}

Adresa Osoba::getTrvaleBydliste() const
{
	return this->trvaleBydliste;
}

Datum Osoba::getDatumNarozeni() const
{
	return this->datumNarozeni;
}


std::ostream& operator<<(std::ostream& output, const Osoba& o) {
	output << o.getJmeno << " " << o.getPrijmeni << " " << o.getTrvaleBydliste << " " << o.getDatumNarozeni << " " << std::endl;
	return output;
}
std::istream& operator>>(std::istream& input, Osoba& o) {
	input >> o.getJmeno;
	input >> o.getPrijmeni;
	input >> o.getTrvaleBydliste;
	input >> o.getDatumNarozeni;
	return input;
}
