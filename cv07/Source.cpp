#include "Osoba.h"
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

void uloz() {
	ofstream out;

	const int velPole = 3;
	Osoba pole[velPole];
	Datum datum1;
	datum1.setDen = 01;
	datum1.setMesic = 01;
	datum1.setRok = 2000;
	Datum datum2;
	datum2.setDen = 02;
	datum2.setMesic = 02;
	datum2.setRok = 2002;
	Datum datum3;
	datum3.setDen = 03;
	datum3.setMesic = 03;
	datum3.setRok = 2003;

	Adresa adresa1;
	adresa1.setMesto = "Pardubice";
	adresa1.setPsc = 53003;
	adresa1.setUlice = "Zalesn�n�";
	Adresa adresa2;
	adresa2.setMesto = "Ostrava";
	adresa2.setPsc = 53000;
	adresa2.setUlice = "Zelen�";
	Adresa adresa3;
	adresa3.setMesto = "Praha";
	adresa3.setPsc = 53010;
	adresa3.setUlice = "�erven�";

	pole[0].setJmeno = "Kv�toslav";
	pole[0].setPrijmeni = "Tepl�k";
	pole[0].setDatumNarozeni = datum1;
	pole[0].setTrvaleBydliste = adresa1;

	pole[1].setJmeno = "Jan";
	pole[1].setPrijmeni = "Masaryk";
	pole[1].setDatumNarozeni = datum2;
	pole[1].setTrvaleBydliste = adresa2;

	pole[2].setJmeno = "Kuka�ka";
	pole[2].setPrijmeni = "Kuky";
	pole[2].setDatumNarozeni = datum3;
	pole[2].setTrvaleBydliste = adresa3;

	out.open("text.txt");
	if
		(out.is_open()){
		out << velPole;
		out << pole[0];
		out << pole[1];
		out << pole[2];
		out.close();
	}
	else
		cerr <<
		"Soubor se nepodarilo otevrit";
}

void nacti() {
	ifstream in;
	in.open("text.txt");
	int velPole = 0;
	string slovo;
	if (in.is_open()) {
		in >> velPole;
		Osoba* o = new Osoba[velPole];
		for (int i = 0; i < velPole; i++)
		{
			in >> o[i];
			std::cout << o[i] << std::endl;
		}
		delete[]o;

	}
	else {
		cerr <<
			"Soubor se nepodarilo otevrit";
	}
}

void ulozBinarne() {
	ofstream out;
	const int velPole = 3;
	Osoba pole[velPole];
	Datum datum1;
	datum1.setDen = 01;
	datum1.setMesic = 01;
	datum1.setRok = 2000;
	Datum datum2;
	datum2.setDen = 02;
	datum2.setMesic = 02;
	datum2.setRok = 2002;
	Datum datum3;
	datum3.setDen = 03;
	datum3.setMesic = 03;
	datum3.setRok = 2003;

	Adresa adresa1;
	adresa1.setMesto = "Pardubice";
	adresa1.setPsc = 53003;
	adresa1.setUlice = "Zalesn�n�";
	Adresa adresa2;
	adresa2.setMesto = "Ostrava";
	adresa2.setPsc = 53000;
	adresa2.setUlice = "Zelen�";
	Adresa adresa3;
	adresa3.setMesto = "Praha";
	adresa3.setPsc = 53010;
	adresa3.setUlice = "�erven�";

	pole[0].setJmeno = "Kv�toslav";
	pole[0].setPrijmeni = "Tepl�k";
	pole[0].setDatumNarozeni = datum1;
	pole[0].setTrvaleBydliste = adresa1;

	pole[1].setJmeno = "Jan";
	pole[1].setPrijmeni = "Masaryk";
	pole[1].setDatumNarozeni = datum2;
	pole[1].setTrvaleBydliste = adresa2;

	pole[2].setJmeno = "Kuka�ka";
	pole[2].setPrijmeni = "Kuky";
	pole[2].setDatumNarozeni = datum3;
	pole[2].setTrvaleBydliste = adresa3;

	int cislo = 0;
	out.open("osoby.bin", ios::out | ios::binary);
	if (out.is_open())
	{
		out.write((const char*)& velPole, sizeof(velPole));
		for (int i = 0; i < velPole; i++) {
			out.write(pole[i].getJmeno().c_str(), pole[i].getJmeno().size() + 1);
			out.write(pole[i].getPrijmeni().c_str, pole[i].getPrijmeni().size() + 1);
			out.write(pole[i].getTrvaleBydliste().getUlice().c_str(), pole[i].getTrvaleBydliste().getUlice().size() + 1);
			out.write(pole[i].getTrvaleBydliste().getMesto().c_str(), pole[i].getTrvaleBydliste().getMesto().size() + 1);
			cislo = pole[i].getTrvaleBydliste().getPsc();
			out.write((const char*)& cislo, sizeof(cislo));
			cislo = pole[i].getDatumNarozeni().getDen();
			out.write((const char*)& cislo, sizeof(cislo));
			cislo = pole[i].getDatumNarozeni().getMesic();
			out.write((const char*)& cislo, sizeof(cislo));
			cislo = pole[i].getDatumNarozeni().getRok();
			out.write((const char*)& cislo, sizeof(cislo));
		}
	}else
		cerr <<
		"Soubor se nepodarilo otevrit";
}

void nactiBinarne() {
	int velPole = 0;
	
	Osoba* pole = new Osoba[velPole];
	string str{};
	int cislo = 0;
	Adresa adresa = Adresa{};
	Datum datum = Datum{};
	ifstream in{};
	in.open("osoby.bin", ios::in | ios::binary);
	if (in.is_open())
	{
		in.read((char*)& velPole, sizeof(velPole));
		for (int i = 0; i < velPole; i++) {
			getline(in, str, '\0');
			pole[i].setJmeno(str);
			getline(in, str, '\0');
			pole[i].setPrijmeni(str);
			getline(in, str, '\0');
			adresa.setUlice(str);
			getline(in, str, '\0');
			adresa.setMesto(str);
			in.read((char*)& cislo, sizeof(cislo));
			adresa.setPsc(cislo);
			pole[i].setTrvaleBydliste(adresa);
			in.read((char*)& cislo, sizeof(cislo));
			datum.setDen(cislo);
			in.read((char*)& cislo, sizeof(cislo));
			datum.setMesic(cislo);
			in.read((char*)& cislo, sizeof(cislo));
			datum.setRok(cislo);
			pole[i].setDatumNarozeni(datum);
		}
	}
	else
	{
		cerr << "Soubor se nepodarilo otevrit.";
	}
	for (int i = 0; i < velPole; i++)
	{
		cout << pole[i] << endl;
	}
	delete[] pole;
	in.close();
}

int main() {
	uloz();
	nacti();
	ulozBinarne();
	nactiBinarne();

	return 0;
}