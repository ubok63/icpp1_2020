#ifndef OSOBA_H
#define OSOBA_H
#include "Adresa.h"
#include "Datum.h"
#include <iostream>
#include <ostream>
#include <fstream>

struct Osoba
{
private:
	std::string jmeno;
	std::string prijmeni;
	Adresa trvaleBydliste;
	Datum datumNarozeni;
	
public:
	Osoba();
	Osoba(std::string jmeno, std::string prijmeni, Adresa trvaleBydliste, Datum datumNarozeni);
	~Osoba();
	void setJmeno(std::string jmeno);
	void setPrijmeni(std::string prijmeni);
	void setTrvaleBydliste(Adresa trvaleBydliste);
	void setDatumNarozeni(Datum datumNarozeni);
	std::string getJmeno() const;
	std::string getPrijmeni() const;
	Adresa getTrvaleBydliste() const;
	Datum getDatumNarozeni() const;
};

std::ostream& operator<<(std::ostream& output, const Osoba& o);
std::istream& operator>>(std::istream& input, Osoba& o);


#endif OSOBA_H
