#include "Matice.h"




template<typename T>
Matice<T>::~Matice(){
	for (int i = 0; i < radky; i++){
		delete[] hodnoty[i];
	}
	delete Matice;
}

template<typename T>
void Matice<T>::nastav(int radek, int sloupec, T hodnota){
	if (radky <= radek || sloupce <= sloupec) {
		throw std::exception("�patn� hodnoty");
	}
	else {
		hodnoty[radek][sloupec] = hodnota;
	}
}

template<typename T>
void Matice<T>::nastavZ(T * pole){
	for (int i = 0; i < radky; i++){
		for (int j = 0; j < sloupce; j++){
			hodnoty[i][j] = pole[radky + sloupce];
		}
	}
}

template<typename T>
template<typename R>
Matice<R> Matice<T>::pretypuj() const{
	Matice<T> matice = Matice(radky, sloupce);
	for (int i = 0; i < radky; i++){
		for (int j = 0; j < sloupce; j++){
			matice.nastav(i, j, static_cast<R>(hodnoty[i][j]));
		}
	}
	return matice;
}

template<typename T>
Matice<T> Matice<T>::transpozice() const{
	Matice<T> m = Matice(radky, sloupce);

	for (int i = 0; i < radky; i++){
		for (int j = 0; j < sloupce; j++){
			m.hodnoty[j][i] = hodnoty[i][j];
		}
	}
	return m;
}

template<typename T>
Matice<T> Matice<T>::soucin(const Matice & m) const{
	if (m.radky != sloupce) {
		throw std::exception("nelze n�sobit");
	}
	else {
		Matice<T> matice = Matice(radky, sloupce);
		for (int i = 0; i < radky; i++){
			for (int j = 0; j < sloupce; j++){
				matice.hodnoty[i][j] = 0;
			}
		}

		for (int i = 0; i < radky; i++){
			for (int j = 0; j < matice.sloupce; j++){
				for (int k = 0; k < sloupce; k++)
					matice.hodnoty[i][j] += hodnoty[i][k] * m.hodnoty[k][j];
			}
		}
		return matice;
	}
}

template<typename T>
Matice<T> Matice<T>::soucin(T skalar){
	Matice<T> matice = Matice(radky, sloupce);
	for (int i = 0; i < radky; i++){
		for (int j = 0; j < sloupce; j++){
			matice.hodnoty[i][j] = matice.hodnoty[i][j] * skalar;
		}
	}
	return matice;
}

template<typename T>
Matice<T> Matice<T>::soucet(const Matice & m) const{
	Matice<T> matice = Matice(radky, sloupce);
	for (int i = 0; i < radky; i++){
		for (int j = 0; j < sloupce; j++){
			matice[i][j] = hodnoty[i][j] + m.values[i][j];
		}
	}
	return matice;
}

template<typename T>
Matice<T> Matice<T>::soucet(T skalar) const{
	Matice<T> matice = Matice(radky, sloupce);
	for (int i = 0; i < radky; i++){
		for (int j = 0; j < sloupce; j++){
			matice[i][j] = hodnoty[i][j] + skalar;
		}
	}
	return matice;
}

template<typename T>
bool Matice<T>::jeShodna(const Matice & m) const{
	for (int i = 0; i < radky; i++){
		for (int j = 0; j < sloupce; j++){
			if (hodnoty[i][j] != m.hodnoty[i][j]) {
				return false;
			}
		}
	}
	return true;
}

template<typename T>
void Matice<T>::Vypis() const{
	for (int i = 0; i < radky; i++){
		for (int j = 0; j < sloupce; j++){
			std::cout << hodnoty[i][j] << " " << std::endl;
		}
		std::cout << "\n" << std::endl;
	}
}


