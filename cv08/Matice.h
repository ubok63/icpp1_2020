#ifndef MATICE_H
#define MATICE_H
template<typename T>
class Matice{
private:
	T** hodnoty;
	int radky;
	int sloupce;

public:
	Matice(int radky, int sloupce);
	Matice(const Matice<T>& m);
	~Matice();

	void nastav(int radek, int sloupec, T hodnota);
	void nastavZ(T* pole);
	template<typename R>
	Matice<R> pretypuj() const;
	Matice transpozice() const;
	Matice soucin(const Matice& m) const;
	Matice soucin(T skalar);
	Matice soucet(const Matice& m) const;
	Matice soucet(T skalar) const;
	bool jeShodna(const Matice& m) const;
	void Vypis() const;
};

template<typename T>
Matice<T>::Matice(int radky, int sloupce){
	this->radky = radky;
	this->sloupce = sloupce;
	hodnoty = new T * [radky];
	for (int i = 0; i < radky; i++){
		hodnoty[i] = new T[sloupce];
	}

	for (int i = 0; i < radky; i++) {
		for (int j = 0; j < sloupce; j++){
			hodnoty[i][j] = nullptr;
		}
	}
}


template<typename T>
Matice<T>::Matice(const Matice& m){
	this->radky = m.radky;
	this->sloupce = m.sloupce;
	hodnoty = new T * [radky];
	for (int i = 0; i < radky; i++){
		hodnoty[i] = new T[sloupce];
	}
	for (int i = 0; i < radky; i++) {
		for (int j = 0; j < sloupce; j++){
			hodnoty[i][j] = m.hodnoty[i][j];
		}
	}
}

#endif MATICE_H