#ifndef __ROSTOUCIKONTEJNER_H
#define __ROSTOUCIKONTEJNER_H
#endif
#include <stdexcept>
#include <iostream>
#include <string>
#include <exception>

template<typename TypDat, int PocatecniVelikost = 5, int RostouciKoeficient = 2>
class RostouciKontejner {
private:
	TypDat* _pole;
	unsigned _velikostPole;
	unsigned _pocetPlatnychPrvku;
	bool jeMistoVPoli() const;
	void zvetsiPole();
public:
	unsigned getVelikostPole() const;
	RostouciKontejner() {
		this->_velikostPole = PocatecniVelikost;
		this->_pocetPlatnychPrvku = 0;
		this->_pole = new TypDat [this->_velikostPole];
	}
	~RostouciKontejner() {
		delete[] _pole;
	}
	void pridej(const TypDat& o);
	TypDat& operator[](int index);
	TypDat operator[](int index) const;
	unsigned int pocet() const;
};
template<typename TypDat, int PocatecniVelikost, int RostouciKoeficient>
bool RostouciKontejner<TypDat, PocatecniVelikost, RostouciKoeficient>::jeMistoVPoli() const {
	return this->_pocetPlatnychPrvku < this->_velikostPole;
}

template<typename TypDat, int PocatecniVelikost, int RostouciKoeficient>
void RostouciKontejner<TypDat, PocatecniVelikost, RostouciKoeficient>::zvetsiPole() {
	if (this->_velikostPole == 0) {
		this->_velikostPole = 1;
	}
	TypDat* temp = new TypDat[this->_velikostPole * RostouciKoeficient];
	for (int i = 0; i < this->_velikostPole; i++) {
		temp[i] = this->_pole[i];
	}
	this->_velikostPole = this->_velikostPole * RostouciKoeficient;
	this->_pole = temp;
	delete[] this->temp;
}

template<typename TypDat, int PocatecniVelikost, int RostouciKoeficient>
inline unsigned const RostouciKontejner<TypDat, PocatecniVelikost, RostouciKoeficient>::getVelikostPole() const
{
	return this->_velikostPole;
}

template<typename TypDat, int PocatecniVelikost, int RostouciKoeficient>
void RostouciKontejner<TypDat, PocatecniVelikost, RostouciKoeficient>::pridej(const TypDat& data) {
	if (!jeMistoVPoli()) {
		zvetsiPole();
	}
	this->_pole[this->_pocetPlatnychPrvku] = data;
	this->_pocetPlatnychPrvku++;
}

template<typename TypDat, int PocatecniVelikost, int RostouciKoeficient>
TypDat& RostouciKontejner<TypDat, PocatecniVelikost, RostouciKoeficient>::operator[](int index) {
	if (this->_pole[index] == NULL) {
		throw std::out_of_range("CHYBA - Pristup na neplatny index");
	}

	return this->_pole[index];
}

template<typename TypDat, int PocatecniVelikost, int RostouciKoeficient>
TypDat RostouciKontejner<TypDat, PocatecniVelikost, RostouciKoeficient>::operator[](int index) const {
	if (this->_pocetPlatnychPrvku[index] == NULL) {
		throw std::out_of_range("CHYBA - Pristup na neplatny index");
	}
	return &this->_pole[index];
}

template<typename TypDat, int PocatecniVelikost, int RostouciKoeficient>
unsigned int RostouciKontejner<TypDat, PocatecniVelikost, RostouciKoeficient>::pocet() const {
	return this->_pocetPlatnychPrvku;
}
