#include <string>
#include "RostouciKontejner.h"
using namespace std;
int main() {
	try {
		RostouciKontejner<string, 8> kontejnerString{};
		string s = "prvni radek";
		string s2 = "druhy radek";
		string s3 = "treti radek";
		string s4 = "vlozeny radek";
		kontejnerString.pridej(s);
		kontejnerString.pridej(s2);
		kontejnerString.pridej(s3);
		cout << "Vypis kontejneru string:" << endl;
		cout << "Index v poli: 0 "  << kontejnerString[0] << endl;
		cout << "Index v poli: 1 " << kontejnerString[1] << endl;
		cout << "Index v poli: 2 " << kontejnerString[2] << endl;
		cout << endl;

		RostouciKontejner<int, 1> kontejnerInt{};
		int i = 54;
		kontejnerInt.pridej(i);
		kontejnerInt.pridej(++i);
		kontejnerInt.pridej(++i);

		cout << "Vypis kontejneru int:" << endl;
		cout << kontejnerInt[0] << endl;
		cout << kontejnerInt[1] << endl;
		cout << kontejnerInt[2] << endl;
		cout << endl;


		RostouciKontejner<double, 0> kontejnerDouble{};
		double d = 5.14;
		kontejnerDouble.pridej(d);
		kontejnerDouble.pridej(++d);
		kontejnerDouble.pridej(++d);
		kontejnerDouble.pridej(++d);
		kontejnerDouble.pridej(++d);
		kontejnerDouble.pridej(++d);
		cout << "Vypis kontejneru double:" << endl;
		cout << kontejnerDouble[0] << endl;
		cout << kontejnerDouble[1] << endl;
		cout << kontejnerDouble[2] << endl;
		cout << kontejnerDouble[3] << endl;
		cout << kontejnerDouble[4] << endl;
		cout << kontejnerDouble[5] << endl;
		cout << endl;


		RostouciKontejner<float, 0> kontejnerFloat{};
		float k = 78.12;
		kontejnerFloat.pridej(k);
		kontejnerFloat.pridej(++k);
		cout << "Vypis kontejneru float:" << endl;
		cout << kontejnerFloat[0] << endl;
		cout << kontejnerFloat[1] << endl;
		cout << endl;


		RostouciKontejner<char, 0> kontejnerChar{};
		char c = 'j';
		kontejnerChar.pridej(c);
		cout << "Vypis kontejneru char:" << endl;
		cout << kontejnerChar[0] << endl;
		cout << endl;

		RostouciKontejner<bool, 0> kontejnerBool{};
		bool b = true;
		kontejnerBool.pridej(b);
		cout << "Vypis kontejneru bool:" << endl;
		cout << kontejnerBool[0] << endl;
		cout << endl;

		RostouciKontejner<const char*, 0> kontejnerCharHvezdicka{};
		const char* ch = "kekw";
		kontejnerCharHvezdicka.pridej(ch);
		kontejnerCharHvezdicka.pridej(ch);
		cout << "Vypis kontejneru char*:" << endl;
		cout << kontejnerCharHvezdicka[0] << endl;
		cout << kontejnerCharHvezdicka[1] << endl;
		cout << endl;


	}
	catch (const std::exception & e) {
		std::cout << e.what() << std::endl;
	}
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	return 0;
}