#include "TelefoniSeznam.h"
#include "Osoba.h"



Model::TelefoniSeznam::TelefoniSeznam()
{
	zacatek = nullptr;
}

Model::TelefoniSeznam::~TelefoniSeznam()
{
	PrvekSeznam* prvekSeznam = zacatek;
	while (prvekSeznam != nullptr) {
		PrvekSeznam* tmp = prvekSeznam->dalsi;
		delete prvekSeznam;
		prvekSeznam = tmp;
	}
}

void Model::TelefoniSeznam::pridejOsobu(const Entity::Osoba &os)
{
	PrvekSeznam* prvek = new PrvekSeznam();
	prvek->data = *os;
	prvek->dalsi = zacatek;
	zacatek = prvek;
}

std::string Model::TelefoniSeznam::najdiTelefon(std::string jmeno) const
{
	if (jmeno.size() == 0) {
		throw std::invalid_argument("prazdny parametr");
	}
	PrvekSeznam* prvekSeznam = zacatek;
	while (prvekSeznam != nullptr) {
		if (prvekSeznam->data.getJmeno() == jmeno) {
			return prvekSeznam->data.getTelefon();
		}
			prvekSeznam = prvekSeznam->dalsi;
	}
	throw std::invalid_argument("toto neexistuje");

}

std::string Model::TelefoniSeznam::najdiTelefon(int id) const
{
	if (id < 0) {
		throw std::invalid_argument("prazdny parametr");
	}
	PrvekSeznam* prvekSeznam = zacatek;
	while (prvekSeznam != nullptr) {
		if (prvekSeznam->data.getId() == id) {
			return prvekSeznam->data.getTelefon();
		}
		prvekSeznam = prvekSeznam->dalsi;
	}
	throw std::invalid_argument("toto neexistuje");
}

Model::TelefoniSeznam::PrvekSeznam::PrvekSeznam()
{
}

Model::TelefoniSeznam::PrvekSeznam::~PrvekSeznam()
{
}
