#ifndef OSOBA_H
#define OSOBA_H
#include <string>

namespace Entity {
	class Osoba
	{
	public:
		Osoba();
		Osoba(std::string jmeno, std::string telefon, int id);
		~Osoba();
		std::string getJmeno() const;
		std::string getTelefon() const;
		int getId() const;

	private:
		std::string jmeno;
		std::string telefon;
		int id;
	};
}
#endif OSOBA_H