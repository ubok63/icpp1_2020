#include "Osoba.h"
#include "TelefoniSeznam.h"
#include <iostream>

int main() {

	Model::TelefoniSeznam* ts = new Model::TelefoniSeznam();
	ts->pridejOsobu(Entity::Osoba("jmeno", "777888999", 10));
	ts->pridejOsobu(Entity::Osoba("jmeno2", "111222333", 11));
	ts->pridejOsobu(Entity::Osoba("jmeno3", "444555666", 12));

	try {
		ts->najdiTelefon(11);
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	}

	return 0;
}