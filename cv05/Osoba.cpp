#include "Osoba.h"







Entity::Osoba::Osoba()
{
}

Entity::Osoba::Osoba(std::string jmeno, std::string telefon, int id) : jmeno(jmeno), telefon(telefon), id(id)
{
}

Entity::Osoba::~Osoba()
{
}

std::string Entity::Osoba::getJmeno() const
{
	return jmeno;
}

std::string Entity::Osoba::getTelefon() const
{
	return telefon;
}

int Entity::Osoba::getId() const
{
	return id;
}
