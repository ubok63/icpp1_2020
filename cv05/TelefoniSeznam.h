#ifndef TELEFONISEZNAM_H
#define TELEFONISEZNAM_H
#include <string>
#include"Osoba.h"

namespace Model {
	class TelefoniSeznam
	{

		class PrvekSeznam {
		public:
			PrvekSeznam();
			~PrvekSeznam();
			Entity::Osoba data;
			PrvekSeznam* dalsi;
		};
	public:
		TelefoniSeznam();
		~TelefoniSeznam();
		void pridejOsobu(const Entity::Osoba &o);
		std::string najdiTelefon(std::string jmeno) const;
		std::string najdiTelefon(int id)const;
	private:
		PrvekSeznam* zacatek;
	};
}

#endif TELEFONISEZNAM_H