#include "Potrubi.h"
#include <stdexcept>


Potrubi::Potrubi(int velikost){
	PolePrvku = new APotrubniPrvek**[velikost];
	for (int i = 0; i < velikost; i++){
		PolePrvku[i] = new APotrubniPrvek*[velikost];
	}

	for (int i = 0; i < velikost; i++){
		for (int j = 0; j < velikost; j++){
			PolePrvku[i][j] = nullptr;
		}
	}
}


Potrubi::~Potrubi(){
	for (int i = 0; i < velikost; i++){
		for (int j = 0; j < velikost; j++){
			delete PolePrvku[i][j];
		}
	}

	for (int i = 0; i < velikost; i++){
		delete PolePrvku[i];
	}
	delete PolePrvku;
}

VychodZapad::VychodZapad(int x, int y) {
	typ = 1;
	this->_x = x;
	this->_y = y;
}

SeverJih::SeverJih(int x, int y) {
	typ = 2;
	this->_x = x;
	this->_y = y;
}

SeverJihVychodZapad::SeverJihVychodZapad(int x, int y) {
	typ = 3;
	this->_x = x;
	this->_y = y;
}

Nic::Nic(int x, int y) {
	typ = 4;
	this->_x = x;
	this->_y = y;
}
JihVychodZapad::JihVychodZapad(int x, int y) {
	typ = 5;
	this->_x = x;
	this->_y = y;
}

bool VychodZapad::JeKorektneZapojen(const IPotrubi * potrubi, APotrubniPrvek* prvek) const{
	if (potrubi->DejPrvek(prvek->_x, prvek->_y - 1) != nullptr && potrubi->DejPrvek(prvek->_x, prvek->_y + 1) != nullptr
		&& potrubi->DejPrvek(prvek->_x, prvek->_y - 1)->typ != 2 && potrubi->DejPrvek(prvek->_x, prvek->_y + 1)->typ != 2) {
		return true;
	}
	else {
		return false;
	}
}

bool SeverJih::JeKorektneZapojen(const IPotrubi * potrubi, APotrubniPrvek* prvek) const{
	if (potrubi->DejPrvek(prvek->_x - 1, prvek->_y) != nullptr && potrubi->DejPrvek(prvek->_x + 1, prvek->_y) != nullptr
		&& potrubi->DejPrvek(prvek->_x - 1, prvek->_y)->typ != 1 && potrubi->DejPrvek(prvek->_x - 1, prvek->_y)->typ != 1) {
		return true;
	}
	else {
		return false;
	}
}

bool SeverJihVychodZapad::JeKorektneZapojen(const IPotrubi * potrubi, APotrubniPrvek* prvek) const{
	if (potrubi->DejPrvek(prvek->_x - 1, prvek->_y) != nullptr
		&& potrubi->DejPrvek(prvek->_x, prvek->_y + 1) != nullptr
		&& potrubi->DejPrvek(prvek->_x + 1, prvek->_y) != nullptr
		&& potrubi->DejPrvek(prvek->_x, prvek->_y - 1) != nullptr
		&& potrubi->DejPrvek(prvek->_x - 1, prvek->_y)->typ != 1
		&& potrubi->DejPrvek(prvek->_x + 1, prvek->_y)->typ != 1
		&& potrubi->DejPrvek(prvek->_x, prvek->_y - 1)->typ != 2
		&& potrubi->DejPrvek(prvek->_x, prvek->_y + 1)->typ != 2
		&& potrubi->DejPrvek(prvek->_x - 1, prvek->_y)->typ != 5) {
		return true;
	}
	else {
		return false;
	}
}

bool Nic::JeKorektneZapojen(const IPotrubi * potrubi, APotrubniPrvek* prvek) const{
	return true;
}

bool JihVychodZapad::JeKorektneZapojen(const IPotrubi * potrubi, APotrubniPrvek* prvek) const{
	if (potrubi->DejPrvek(prvek->_x + 1, prvek->_y) != nullptr
		&& potrubi->DejPrvek(prvek->_x, prvek->_y - 1) != nullptr
		&& potrubi->DejPrvek(prvek->_x, prvek->_y + 1) != nullptr
		&& potrubi->DejPrvek(prvek->_x + 1, prvek->_y)->typ != 1
		&& potrubi->DejPrvek(prvek->_x + 1, prvek->_y)->typ != 5
		&& potrubi->DejPrvek(prvek->_x, prvek->_y - 1)->typ != 2
		&& potrubi->DejPrvek(prvek->_x, prvek->_y + 1)->typ != 2) {
		return true;
	}
	return false;
}

void Potrubi::vlozPrvek(APotrubniPrvek * prvek) const{
	if (prvek->_x < velikost && prvek->_y < velikost) {
		PolePrvku[prvek->_x][prvek->_y] = prvek;
	}
	else {
		throw std::out_of_range("prvek mimo pole");
	}
}

const APotrubniPrvek * Potrubi::DejPrvek(int x, int y) const{
	if (x < velikost && y < velikost) {
		return PolePrvku[x][y];
	}
	else {
		throw std::out_of_range("souĝadnice jsou mimo pole");
	}
}

bool Potrubi::JePotrubiOk() const {
	for (int i = 0; i < velikost; i++) {
		for (int j = 0; j < velikost; j++) {
			if (PolePrvku[i][j] != nullptr) {
				if (PolePrvku[i][j]->JeKorektneZapojen(this, PolePrvku[i][j])) {
					return false;
				}
			}
		}
	}
	return true;
}

const int Potrubi::getVelikost() {
	return velikost;
}

APotrubniPrvek::~APotrubniPrvek() {
}
