#ifndef POTRUBI_H
#define POTRUBI_H

struct APotrubniPrvek;
struct IPotrubi {
public: 
	virtual void vlozPrvek(APotrubniPrvek* prvek) const = 0;
	virtual ~IPotrubi() {}
	virtual const APotrubniPrvek* DejPrvek(int x, int y) const = 0;
	virtual bool JePotrubiOk() const = 0;
};

struct Potrubi : IPotrubi {
private:
	int velikost;
	APotrubniPrvek*** PolePrvku;
public:
	Potrubi(int velikost);
	~Potrubi();
	void vlozPrvek(APotrubniPrvek* prvek) const override;
	const APotrubniPrvek* DejPrvek(int x, int y) const override;
	bool JePotrubiOk() const override;
	const int getVelikost();
};

struct APotrubniPrvek {
	virtual ~APotrubniPrvek();
	virtual bool JeKorektneZapojen(const IPotrubi* potrubi, APotrubniPrvek* prvek) const = 0;
	int _x;
	int _y;
	int typ;
};

struct VychodZapad : APotrubniPrvek {
	VychodZapad(int x, int y);
	~VychodZapad(){}
	bool JeKorektneZapojen(const IPotrubi* potrubi, APotrubniPrvek* prvek) const override;
};

struct SeverJih : APotrubniPrvek {
	SeverJih(int x, int y);
	~SeverJih() {}
	bool JeKorektneZapojen(const IPotrubi* potrubi, APotrubniPrvek* prvek) const override;
};

struct SeverJihVychodZapad : APotrubniPrvek {
	SeverJihVychodZapad(int x, int y);
	~SeverJihVychodZapad(){}
	bool JeKorektneZapojen(const IPotrubi* potrubi, APotrubniPrvek* prvek) const override;
};

struct Nic : APotrubniPrvek {
	Nic(int x, int y);
	~Nic(){}
	bool JeKorektneZapojen(const IPotrubi* potrubi, APotrubniPrvek* prvek) const override;
};

struct JihVychodZapad : APotrubniPrvek {
	JihVychodZapad(int x, int y);
	~JihVychodZapad(){}
	bool JeKorektneZapojen(const IPotrubi* potrubi, APotrubniPrvek* prvek) const override;
};
#endif POTRUBI_H