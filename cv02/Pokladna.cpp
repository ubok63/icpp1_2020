#include "Pokladna.h"
#include "Uctenka.h"

#include <stdexcept>

Pokladna::Pokladna()
{
	this->poleUctenek = new Uctenka[10];
	this->pocetVydanychUctenek = 0;
}
Pokladna::~Pokladna()
{
	delete[] poleUctenek;
}
int Pokladna::citacId = 1000;


int Pokladna::getCitacId() {
	return citacId;
}

Uctenka& Pokladna::vystavUctenku(double x, double y) {
	
		if (Pokladna::pocetVydanychUctenek < 10) {
			poleUctenek[pocetVydanychUctenek].setCastka(x);
			poleUctenek[pocetVydanychUctenek].setDph(y);
			poleUctenek[pocetVydanychUctenek].setCisloUctenky(citacId);
			citacId++;
			return poleUctenek[pocetVydanychUctenek++];
		}
	
	else {
		throw std::overflow_error("Cash register is full!");
	}
	
}

Uctenka& Pokladna::dejUctenku(int x) {
	for (int i = 0; i < pocetVydanychUctenek; i++) {
			if (poleUctenek[i].getCisloUctenky() == x) {
				return poleUctenek[i];
			}
	}
	return poleUctenek[0];
}

double Pokladna::dejCastku() const{
	double result = 0;
	for (int i = 0; i < pocetVydanychUctenek; i++) {
		if (poleUctenek[i].getCisloUctenky() >= 1000) {
			result += poleUctenek[i].getCastka();
		}
	}
	return result;
}


double Pokladna::dejCastkuVcDph() const{
	double result = 0;
	for (int i = 0; i < pocetVydanychUctenek; i++) {
		if (poleUctenek[i].getCisloUctenky() >= 1000) {
			result += (poleUctenek[i].getCastka() * (1 + poleUctenek[i].getDph()));
		}
		else {
			break;
		}
	}
	return result;
}