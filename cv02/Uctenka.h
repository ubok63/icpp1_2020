#ifndef UCTENKA_H
#define UCTENKA_H

class Uctenka {
private:
	int cisloUctenky;
	double castka;
	double dph;

public:
	Uctenka();


	void setCisloUctenky(int s);

	int getCisloUctenky();

	void setCastka(double s);

	double getCastka();

	void setDph(double s);

	double getDph();

};

#endif UCTENKA_H