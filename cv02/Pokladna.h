#ifndef POKLADNA_H
#define POKLADNA_H
#include "Uctenka.h"

using namespace std;

class Pokladna {
private:
	static int citacId;
	Uctenka* poleUctenek = nullptr;
	int pocetVydanychUctenek;
public:
	
	Pokladna();

	~Pokladna();
	
	int getCitacId();

	Uctenka& vystavUctenku(double x, double y);

	Uctenka& dejUctenku(int x);

	double dejCastku() const;

	double dejCastkuVcDph() const;
};



#endif POKLADNA_H

