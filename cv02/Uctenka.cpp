#include "Uctenka.h"
#include "Pokladna.h"

Uctenka::Uctenka() {
	this->castka = 0;
	this->cisloUctenky = 0;
	this->dph = 0;
}


void Uctenka::setCisloUctenky(int s) {
	cisloUctenky = s;
}

int Uctenka::getCisloUctenky() {
	return cisloUctenky;
}

void Uctenka::setCastka(double s) {
	castka = s;
}

double Uctenka::getCastka() {
	return castka;
}

void Uctenka::setDph(double s) {
	dph = s;
}

double Uctenka::getDph() {
	return dph;
}
