#include <iostream>
#include "Uctenka.h"
#include "Pokladna.h"

using namespace std;

int main() {

	Pokladna pokladna1{};

	pokladna1.vystavUctenku(200, 0.2);
	pokladna1.vystavUctenku(264, 0.2);
	pokladna1.vystavUctenku(586, 0.2);
	pokladna1.vystavUctenku(1000, 0.2);

	Uctenka& u = pokladna1.dejUctenku(1002);

	int castka = pokladna1.dejCastku();

	int castkaVcDph = pokladna1.dejCastkuVcDph();

	cout << u.getCastka() << "\n" << u.getCisloUctenky() << "\n" << endl;

	cout << "castka: " << castka << "\n" << endl;

	cout << "castka vcetne dph: " << castkaVcDph << endl;

	system("pause");
	return 0;
}