#ifndef OBJECT_H
#define OBJECT_H


class Object
{
public:
	Object(int id);
	virtual ~Object();

	void setX(double x);
	void setY(double y);
	double getX();
	double getY();
	int getId();

private:
	int id;
	double x;
	double y;
};

#endif OBJECT_H