#ifndef MONSTRUM_H
#define MONSTRUM_H
#include "PohyblivyObject.h"
class Monstrum :
	public PohyblivyObject
{
public:
	Monstrum(int id);
	~Monstrum();
	void setHp(int hp);
	void setMaxHp(int maxHp);
	int getHp() const;
	int getMaxHp() const;

private:
	int hp;
	int maxHp;
};

#endif MONSTRUM_H