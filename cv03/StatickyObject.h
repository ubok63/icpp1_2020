#ifndef STATICKY_OBJECT_H
#define STATICKY_OBJECT_H
#include "Object.h"

enum TypPrekazky {
	Skala,
	MalaRostlina,
	VelkaRostlina
};

class StatickyObject :
	public Object
{
public:
	StatickyObject(int id, TypPrekazky typPrekazky);
	~StatickyObject();
	TypPrekazky typPrekazky;
	TypPrekazky getTypPrekazky();
};

#endif STATICKY_OBJECT_H