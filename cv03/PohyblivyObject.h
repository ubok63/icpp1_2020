#pragma once
#include "Object.h"
class PohyblivyObject :
	public Object
{
public:
	PohyblivyObject(int id);
	~PohyblivyObject();
	void setUhelNatoceni(double uhelNatoceni);
	double getUheNatoceni();

private:
	double uhelNatoceni;
};

