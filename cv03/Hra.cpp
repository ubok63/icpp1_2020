#include "Hra.h"
#include "PohyblivyObject.h"
#include "Object.h"
#include "StatickyObject.h"
#include <stdio.h>
#include <math.h>

Hra::Hra()
{
	this->objekty = new Objekt * [10];
	int max = 10;
	int pocet = 0;
}

Hra::~Hra()
{
	for (int i = 0; i < pocet; i++)
	{
		delete objekty[i];
	}
	delete[] this->objekty;
}

void Hra::vlozObjekt(Objekt* o)
{
	if (max != pocet) {
		objekty[pocet] = o;
		pocet++;
	}
	else {
		Objekt** tmp = new Objekt * [max * 2];
		max *= 2;
		for (int i = 0; i < pocet; i++)
		{
			tmp[i] = objekty[i];
		}
		tmp[pocet] = o;
		pocet++;
		objekty = tmp;
		delete[] tmp;
	}
}


const int* Hra::najdiIdStatickychObjektu(double xMin, double xMax, double yMin, double yMax)
{
	int pocetStatickychObjektu = 0;
	for (int i = 0; i < pocetPrvku; i++)
	{
		Object* o = dynamic_cast<StatickyObject*>(objekty[i]);
		if (o != nullptr) {
			pocetStatickychObjektu++;
		}
	}

	if (pocetStatickychObjektu == 0) {
		return nullptr;
	}

	int* pole = new int[pocetStatickychObjektu];
	int pocet = 0;
	for (int i = 0; i < pocetPrvku; i++)
	{
		Object* o = dynamic_cast<StatickyObject*>(objekty[i]);
		if (o != nullptr) {
			if (objekty[i]->getX() >= xMin && objekty[i]->getX() <= xMax && objekty[i]->getY() >= yMin && objekty[i]->getY() <= yMax) {
				pole[pocet++] = objekty[i]->getId();
			}
		}
	}

	return pole;
}

PohyblivyObject** Hra::najdiPohybliveObjektyVOblasti(double x, double y, double r) const
{
	int pocetPohyblivychObjektu = 0;
	for (int i = 0; i < pocetPrvku; i++)
	{
		Object* o = dynamic_cast<PohyblivyObject*>(objekty[i]);
		if (o != nullptr) {
			pocetPohyblivychObjektu++;
		}
	}

	if (pocetPohyblivychObjektu == 0) {
		return nullptr;
	}

	PohyblivyObject** pole = new PohyblivyObject*[pocetPohyblivychObjektu];
	int pocet = 0;
	for (int i = 0; i < pocetPrvku; i++)
	{
		PohyblivyObject* o = dynamic_cast<PohyblivyObject*>(objekty[i]);
		if (o != nullptr) {
			if (pow(objekty[i]->getX() - x, 2) + (objekty[i]->getY() - y, 2) <= pow(r, 2)) {
				pole[pocet++] = o;
			}
		}
	}
	return pole;
}

PohyblivyObject ** Hra::najdiPohybliveObjektyVOblasti(double x, double y, double r, double uMin, double uMax) const
{
	int pocetPohyblivychObjektu = 0;
	for (int i = 0; i < pocetPrvku; i++)
	{
		Object* o = dynamic_cast<PohyblivyObject*>(objekty[i]);
		if (o != nullptr) {
			pocetPohyblivychObjektu++;
		}
	}

	if (pocetPohyblivychObjektu == 0) {
		return nullptr;
	}

	PohyblivyObject** pole = new PohyblivyObject*[pocetPohyblivychObjektu];
	int pocet = 0;
	for (int i = 0; i < pocetPrvku; i++)
	{
		PohyblivyObject* o = dynamic_cast<PohyblivyObject*>(objekty[i]);
		if (o != nullptr) {
			if (pow(objekty[i]->getX() - x, 2) + (objekty[i]->getY() - y, 2) <= pow(r, 2) && o->getUheNatoceni() >= uMin && o->getUheNatoceni() <= uMax) {
				pole[pocet++] = o;
			}
		}
	}
	return pole;
}