#include "Monstrum.h"



Monstrum::Monstrum(int id) : PohyblivyObject(id)
{
}


Monstrum::~Monstrum()
{
}

void Monstrum::setHp(int hp)
{
	this->hp = hp;
}

void Monstrum::setMaxHp(int maxHp)
{
	this->maxHp = maxHp;
}

int Monstrum::getHp() const
{
	return hp;
}

int Monstrum::getMaxHp() const
{
	return maxHp;
}
