#ifndef HRA_H
#define HRA
#include "Object.h"
#include "PohyblivyObject.h"


class Hra
{
public:
	Hra();
	~Hra();
	void vlozObject(Object* o);
	const int* najdiIdStatickychObjektu(double xMin, double xMax, double yMin, double yMax);
	PohyblivyObject** najdiPohybliveObjektyVOblasti(double x, double y, double r) const;
	PohyblivyObject** najdiPohybliveObjektyVOblasti(double x, double y, double r, double uMin, double uMax) const;


private:
	Object** objekty;
	int pocetPrvku = 0;
};

#endif HRA_H