#include <iostream>
#include "Hra.h"
#include "Monstrum.h"
#include "PohyblivyObject.h"
#include "StatickyObject.h"
#include <time.h>

int main() {
	Object* objekt = new StatickyObject{ 1, TypPrekazky::MalaRostlina };
	StatickyObject* so = dynamic_cast<StatickyObject*>(objekt);
	if (so != nullptr)
		std::cout << "objekt je StatickyObjekt(nebo jeho potomek)" << so->getTypPrekazky();
	int counter = 0;
	Hra* h = new Hra();
	time_t startTime = time(nullptr);
	for (int i = 0; i < 10000; i++)
	{
		Object* o;
		if (rand() % 2 == 0) {
			PohyblivyObject* po = new PohyblivyObject(counter++);
			po->setX(rand() % 100 + 100);
			po->setY(rand() % 100 + 100);
			o = po;
		}
		else {
			o = new StatickyObject(i, TypPrekazky::Skala);
		}
		h->vlozObject(o);
	}
	time_t endTime = time(nullptr);
	char* startBuffer = new char[20];
	char* endBuffer = new char[20];
	std::cout << "start time: " << startTime << std::endl
		<< "End time: " << endTime << std::endl;
	system("pause");
	return 0;
}