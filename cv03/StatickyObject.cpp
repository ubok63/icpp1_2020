#include "StatickyObject.h"



StatickyObject::StatickyObject(int id, TypPrekazky typPrekazky) : Object(id)
{
	this->typPrekazky = typPrekazky;
}


StatickyObject::~StatickyObject()
{
}

TypPrekazky StatickyObject::getTypPrekazky()
{
	return typPrekazky;
}
