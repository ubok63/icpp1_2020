#include "Cas.h"
#include <iostream>
void seraditPole(IComparable ** pole, int velikostPole)
{
	for (int i = 0; i < velikostPole; i++)
	{
		for (int j = 0; j < i; j++)
		{
			if (pole[j]->compareTo(pole[i]) == 1) {
				IComparable* tmp = pole[j];
				pole[j] = pole[i];
				pole[i] = tmp;
			}
		}
	}
}

int main(){
	IComparable**pole = new IComparable*[10];
	pole[0] = new Cas(1, 2, 3);
	pole[1] = new Cas(3, 2 , 1);
	pole[2] = new Cas(5, 8, 10);
	pole[3] = new Cas(2, 4, 5);
	pole[4] = new Cas(10, 7, 4);
	Cas* c = new Cas(10, 20, 30);
	for (int i = 0; i < 5; i++)
	{
		std::cout << pole[i] << std::endl;
	}
	seraditPole(pole, 5);
	for (int i = 0; i < 5; i++)
	{
		std::cout << pole[i] << std::endl;
	}
	delete[] pole;
return 0;
}