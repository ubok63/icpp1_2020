#ifndef CAS_H
#define CAS_H
#include "IComparable.h"
class Cas : public IComparable
{
public:
	Cas(int hodiny, int minuty, int sekundy);
	virtual int compareTo(IComparable* obj) const override;
	virtual std::string toString() const override;
private:
	int _hodiny;
	int _minuty;
	int _sekundy;
};

#endif CAS_H
