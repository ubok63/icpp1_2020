#include "Cas.h"
#include "IComparable.h"



int Cas::compareTo(IComparable * obj) const
{
	if (_hodiny == ((Cas*)obj)->_hodiny) {
		if (_minuty == ((Cas*)obj)->_minuty) {
			if (_sekundy == ((Cas*)obj)->_sekundy) {
				return 0;
			}
			else if (_sekundy > ((Cas*)obj)->_sekundy) {
				return 1;
			}
			else {
				return -1;
			}
		}
		else if (_minuty > ((Cas*)obj)->_minuty) {
			return 1;
		}
		else {
			return -1;
		}
	}
	else if (_hodiny > ((Cas*)obj)->_hodiny) {
		return 1;
	}
	else {
		return -1;
	}
}

std::string Cas::toString() const
{
	
	return std::to_string(_hodiny) + ":" + std::to_string(_minuty) + ":" + std::to_string(_sekundy);
}

Cas::Cas(int hodiny, int minuty, int sekundy)
{
	if (hodiny >= 0 && hodiny < 24 && minuty >= 0 && minuty < 60 && sekundy >= 0 && sekundy < 60) {
		_hodiny = hodiny;
		_minuty = minuty;
		_sekundy = sekundy;
	}
	else {
		throw "Chyba";
	}
}